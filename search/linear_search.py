items = input("Enter the items: ").split(",")
items = [int(i) for i in items]
target = int(input("Enter the target item: "))
n = len(items)

#loop implementation
def linearSearchLoop(items, target):
    for item in items:
        if item == target:
            return items.index(item)
    return None


#recursive implementation
def linearSearchRecursion(items, target, n, i = 0):
    if items[i] == items[n] and items[i] != target:
        return None
    elif items[i] == target:
        return i
    else:
        return linearSearchRecursion(items, target, n, i + 1)

print("(Recursive) index: "+str(linearSearchRecursion(items, target, n - 1)))
print("(Loop) index: "+str(linearSearchLoop(items, target)))
