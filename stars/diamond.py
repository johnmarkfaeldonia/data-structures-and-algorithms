def main(size): 
  n = 0
  while n < size:
    n += 1
    print(" " * (size - n) + "*" * (2 * n - 1))
  n = size - 1
  while n > 0:
    print(" " * (size - n) + "*" * (2 * n - 1))
    n -= 1

main(5)