def main(size):
    x = size
    y = size
    while x > 0:
        y = size
        while y > x:
            print(" ", end = "")
            y -= 1
        while y > 0:
            print("*", end = "")
            y -= 1
        x -= 1
        print()

main(5)