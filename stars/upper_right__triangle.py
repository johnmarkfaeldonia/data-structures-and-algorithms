def main(size):
    x = 0
    y = size
    while x < size:
        y = size
        while y > x:
            print(" ", end = "")
            y -= 1
        while y > -1:
            print("*", end = "")
            y -= 1
        x += 1
        print()

main(5)