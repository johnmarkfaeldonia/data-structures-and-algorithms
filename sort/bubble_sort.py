items = input("Enter the items: ").split(",")
items = [int(i) for i in items]
n = len(items)

def bubbleSort(items, n):
    for i in range(n - 1):
        for j in range(n - 1):
            if items[j] > items[j + 1]:
                (items[j], items[j + 1]) = (items[j + 1], items[j])
    return items

items = bubbleSort(items, n)
print(items)


